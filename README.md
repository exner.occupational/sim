# sim

## chipsets

### SIMCom
see https://www.raviyp.com/best-low-cost-4g-gsm-gps-module/

#### SIM900 and SIM800 series
- 2G modules
- EOL (End of live)
SIM800L https://soldered.com/de/produkt/gsm-gprs-modul-sim800l/

#### SIM7600
- bei Qualcomm gefertigt?
- teuer?

SIMCom SIM7020E is 
#### SIM7020E
- a Multi-Band NB-IoT module solution in a SMT format for the European market.
- AT commands mostly compatible with SIM800C
https://www.berrybase.at/nb-iot-hat-fuer-raspberry-pi-eu-version

#### A7670
- bei ASR gefertigt?
- billig?
- ohne GNSS
- mit voice

https://www.waveshare.com/a7670e-cat-1-hat.htm

#### A7672 series
- two primary variants
  1) LASE : Without GNSS & Without Bluetooth
  2) FASE : With GNSS & Bluetooth
- Europe
  A7672E-LASE for without GNSS & Bluetooth
  A7672E-FASE for with GNSS & Bluetooth.

### UBLOX LARA
https://www.u-blox.com/en/product/lara-r6-series

### TELIT 4G modules like LE910

### A9G
- is a complete quad-band GSM/GPRS+GPS module based on the RDA8955 chip.
- The cost reduction of the core chip provides users with a cost-effective IoT solution.

https://www.ribu.at/ai-thinker-a9g-gprs/gps-development-board-antennen

## RPi Config
https://www.youtube.com/watch?v=sv5zYfwl0wk

## Vertrag

Keine Grundgebühr, keine Setupgebühr:
https://www.yesss.at/shop/tarif/classic
